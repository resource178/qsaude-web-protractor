var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var reporter = require('./myReporter.js')
var object = require('./objects.js')

describe('Validar a aba de Plano/Cobertura', function () {
    it('Clicar a aba Beneficiário Cadastro', function () {
        var modulo = element(by.id('menu'));
        modulo.click();

        var central = element(by.css('#menu > ul > li:nth-child(1) > a'));
        central.click();

        var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));
        beneficiario.click();

        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.sleep(2 * 1000); 
        
        //browser.wait(protractor.ExpectedConditions.visibilityOf(element(
         //   by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width')), 5000));
    });

    it('Realizar uma pesquisa de um beneficiário', function () {
        var carteirinha = element(by.css('#float-input-carterinha'));
        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
        var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div.form-group.row > div.col-md-5.col-sm-12.text-md-right.ng-star-inserted > span.labelInfoElegivel.ng-star-inserted > span'));

        carteirinha.click();
        carteirinha.sendKeys('050290000002008');

        btnBuscar.click();

        //expect(beneficiario.getText()).toEqual('Beneficiário Elegível');
        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.sleep(2 * 1000); 
        
        //browser.wait(protractor.ExpectedConditions.visibilityOf(element(
        //    by.css('#float-input-carterinha')), 5000));

    });

    it('Clicar na aba Plano/Cobertura', function () {
        var plano = element(by.css('#ui-tabpanel-2-label > span.ui-tabview-title'));
        plano.click();

        var labelProd = element(by.attr('jhitranslate', 'qSaudeWebApp.beneficiario.plano.produtoContratado'));
        expect(labelProd.getText()).toEqual('Produto Contratado');
        browser.sleep(2 * 1000); 
        
        //browser.wait(protractor.ExpectedConditions.visibilityOf(element(
        //    by.attr('jhitranslate', 'qSaudeWebApp.beneficiario.plano.produtoContratado')), 5000));
        //browser.manage().timeouts().implicitlyWait(1000000);
    });


    it('Validar campos preenchidos', function () {
        var produto = element(by.id('float-input-nome-produto-contratado'));
        expect(produto.getAttribute('ng-reflect-model')).toBe('9229 - MINISTÉRIO PÚBLICO DA U');
        browser.sleep(2 * 1000); 
        
        //browser.manage().timeouts().implicitlyWait(1000000);
        //browser.wait(protractor.ExpectedConditions.visibilityOf(element(
         //   by.attr('id', 'float-input-nome-produto-contratado')), 5000));
    });
});
