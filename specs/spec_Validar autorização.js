var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var report = require('./myReporter.js')
var object = require('./objects.js')

describe('Validar Autorização Q Saude', function () {
    it('Clicar a aba Autorização', function () {

        var modulo = element(by.id('menu'));
        var autorizacao = element(by.attr('jhitranslate', 'global.menu.modulos.autorizacao'));
        var atendimento = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(21) > div > li'));
        var btnBuscar = element(by.attr('jhitranslate', 'qSaudeWebApp.autorizacao.filtro.buscar'));

        modulo.click();
        autorizacao.click();
        atendimento.click();

        expect(btnBuscar).toBeDisplayed();

    });
    it('Realizar Pesquisa', function () {

        var btnBuscar = element(by.attr('jhitranslate', 'qSaudeWebApp.autorizacao.filtro.buscar'));
        var grid = element(by.xpath('/html/body/jhi-main/div[2]/jhi-atendimento-solicitados-main/div/div/div'));

        btnBuscar.click();
        browser.sleep(20 * 1000);
        expect(grid).toBeDisplayed();
    });
    it('Visualizar guia autorizada', function () {

        var btnVisualizar = element(by.xpath('/html/body/jhi-main/div[2]/jhi-atendimento-solicitados-main/div/div/div/jhi-listagem-solicitacao/div[2]/div[1]/table/tbody/tr[1]/td[11]/button'));
        var guia = element(by.attr('jhitranslate', 'qSaudeWebApp.autorizacaoGuiaSADT.sadt.guia.titulo'));
        var autorizado = element(by.xpath('/html/body/jhi-main/div[2]/jhi-autorizacao-guia-sadt/div/div[1]/div/div[2]/div/div'));

        btnVisualizar.click();
        browser.sleep(5 * 1000);
        expect(guia.getText()).toEqual('Detalhes da Guia');
    });
 
});
