var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var helpers = require('protractor-helpers');
var reporter = require('./myReporter.js')
var btnHistorico = element(by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(3)'));
var object = require('./objects.js')

describe('Validar a aba de senha', function () {
    it('Clicar a aba Beneficiário Cadastro', function () {
        var modulo = element(by.id('menu'));
        modulo.click();

        var central = element(by.css('#menu > ul > li:nth-child(1) > a'));
        central.click();

        var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));
        beneficiario.click();

        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width')), 5000));
    });

    it('Realizar uma pesquisa de um beneficiário', function () {
        var carteirinha = element(by.css('#float-input-carterinha'));
        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
        var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div.form-group.row > div.col-md-5.col-sm-12.text-md-right.ng-star-inserted > span.labelInfoElegivel.ng-star-inserted > span'));

        carteirinha.click();
        carteirinha.sendKeys('130090002464002');

        btnBuscar.click();

        //expect(beneficiario.getText()).toEqual('Beneficiário Elegível');
        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#float-input-carterinha')), 5000));
    });

    it('Clicar na aba Senha', function () {
        var senha = element(by.css('#ui-tabpanel-1-label > span.ui-tabview-title'));
        senha.click();

        expect(btnHistorico).toBeDisplayed();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(3)')), 5000));
    });

    it('Validar botão histórico', function () {
        btnHistorico.click();
       
        var protocolo = element(by.css('#ui-tabpanel-1 > jhi-senha > p-dialog > div'));
        expect(protocolo).toBeDisplayed();

        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-tabpanel-1 > jhi-senha > p-dialog > div')), 5000));
    });

    it('Validar botão telefone', function () {
        var fecharHist = element(by.xpath('//*[@id="ui-tabpanel-1"]/jhi-senha/p-dialog/div/div[1]/a'));
        fecharHist.click();

        var btnTelefone = element(by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(4)'));
        btnTelefone.click();

        var grid = element(by.css('body > div.ng-tns-c20-14.ui-overlaypanel.ui-widget.ui-widget-content.ui-corner-all.ui-shadow.ng-trigger.ng-trigger-panelState > div > div:nth-child(2)'));
        expect(grid).toBePresent();

        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(3)')), 5000));
    });

    it('Validar botão anexos', function () {
        var btnAnexo = element(by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(5)'));
        btnAnexo.click();

        browser.manage().timeouts().implicitlyWait(1000000);
        var grid = element(by.attr('jhisortby', 'nomeArquivo'));
        expect(grid).toBePresent();

        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.attr('jhisortby', 'nomeArquivo')), 5000));

    });

    it('Validar botão e-mail', function () {
        var fecharAnexo = element(by.xpath('//*[@id="ui-tabpanel-1"]/jhi-senha/jhi-central-anexo-arquivo-dialog/p-dialog/div/div[1]/a'));
        fecharAnexo.click();

        var btnEmail = element(by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(7)'));
        btnEmail.click();

        browser.manage().timeouts().implicitlyWait(1000000);
        var grid = element(by.css('#ui-tabpanel-1 > jhi-senha > jhi-email > p-dialog > div'));
        expect(grid).toBePresent();

        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-tabpanel-1 > jhi-senha > jhi-email > p-dialog > div')), 5000));
    });

    it('Validar vizualizar', function () {
        var preencherEmail = element(by.css('#mensagem > div > div.ui-editor-content.ql-container.ql-snow > div.ql-editor.ql-blank'));
        preencherEmail.sendKeys('teste');

        var btnEnviar = element(by.xpath('//*[@id="ui-tabpanel-1"]/jhi-senha/jhi-email/p-dialog/div/div[2]/form/div[7]/div/button'));
        btnEnviar.click();

        browser.sleep(5 * 1000);

        var btnVizualizar = element(by.css('#ui-tabpanel-1 > jhi-senha > div > div.table-responsive.ng-star-inserted > table > tbody:nth-child(3) > tr.text-center > td.text-center > div > button:nth-child(6)'));
        btnVizualizar.click();
        browser.get('https://hml-autorizacao.qsaude.com.br/#/visualizar-guia-sadt/1000026');

        var titulo = element(by.attr('jhitranslate', 'qSaudeWebApp.autorizacaoGuiaSADT.sadt.guia.registroANS'));
        expect(titulo).toBePresent();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.attr('jhitranslate', 'qSaudeWebApp.autorizacaoGuiaSADT.sadt.guia.registroANS')), 5000));
    });
});