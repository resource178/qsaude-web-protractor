var Login = require('C:/Users/resource184/AppData/Roaming/npm/node_modules/protractor/example/spec_login.js')
var selecionaOperadora = require('C:/Users/resource184/AppData/Roaming/npm/node_modules/protractor/example/spec_selecionarOperadora.js')

it('Acessar menu Módulo - Central - Beneficiário', function () {

    var modulo = element(by.id('menu'));
    var central = element(by.css('#menu > ul > li:nth-child(1) > a'));
    var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));
    var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

    modulo.click();
    browser.manage().timeouts().implicitlyWait(5000000);

    central.click();
    browser.manage().timeouts().implicitlyWait(5000000);

    beneficiario.click();
    browser.manage().timeouts().implicitlyWait(5000000);

    expect(btnBuscar.getText()).toEqual('Buscar');
    browser.manage().timeouts().implicitlyWait(1000000);

});

it('Realizar uma pesquisa de um beneficiário', function () {

    var carteirinha = element(by.css('#float-input-carterinha'));
    var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
    var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div.form-group.row > div.col-md-5.col-sm-12.text-md-right.ng-star-inserted > span.labelInfoElegivel.ng-star-inserted > span'));

    carteirinha.click();
    browser.manage().timeouts().implicitlyWait(5000000);
    carteirinha.sendKeys('050290000002008');


    btnBuscar.click();
    browser.manage().timeouts().implicitlyWait(5000000);
    browser.manage().timeouts().implicitlyWait(5000000);

    expect(beneficiario.getText()).toEqual('Beneficiário Elegível');
  
});


it('Clicar no botão Novo ', function () {
    var novoBtn = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-3.no-padding-left > jhi-central-atendimento > div > form > div > div:nth-child(8) > div > button:nth-child(1)'));
    novoBtn.click();
    expect(novoBtn.getText()).toEqual('Novo');
    browser.manage().timeouts().implicitlyWait(5000000);

});

it('Preencher os campos e clicar em Salvar ', function () {
    var comboAssunto = element(by.xpath('/html/body/jhi-main/div[2]/jhi-central-main/div/div[2]/jhi-central-atendimento/div/form/div/div[2]/div/div[1]/p-dropdown'));
    var assunto = element(by.xpath("//*[@class = 'ui-dropdown-items ui-dropdown-list ui-widget-content ui-widget ui-corner-all ui-helper-reset ng-tns-c9-1 ng-star-inserted']/li[2]/span"));
    var comboEvento = element(by.xpath("/html/body/jhi-main/div[2]/jhi-central-main/div/div[2]/jhi-central-atendimento/div/form/div/div[3]/p-dropdown"));
    var evento = element(by.xpath("//*[@class = 'ui-dropdown-items ui-dropdown-list ui-widget-content ui-widget ui-corner-all ui-helper-reset ng-tns-c9-2 ng-star-inserted']/li[3]/span"));
    var observacao = element(by.name('descricao'));
    var btnSalvar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div.col-md-3.no-padding-left > jhi-central-atendimento > div > form > div > div.row > div > button:nth-child(2) > span > span'));

    //Preenchendo o combo Assunto
    comboAssunto.click();
    browser.manage().timeouts().implicitlyWait(3000000);
    assunto.click();
    //Preenchendo o combo Evento
    comboEvento.click();
    browser.manage().timeouts().implicitlyWait(3000000);
    evento.click();
    //Preenchendo o campo Observação
    browser.manage().timeouts().implicitlyWait(3000000);
    observacao.click();
    observacao.sendKeys('teste teste teste teste teste');

    //Clicando no botão Salvar
    btnSalvar.click();

    //expect(btnSalvar.getText()).toEqual('Salvar');
    browser.manage().timeouts().implicitlyWait(1000000);


    // expect(element(by.class('ui-growl ui-widget')).getText()).toContain('Um Atendimento foi atualizado');
    botao = element(by.css('/html/body/jhi-main/div[2]/jhi-central-main/div/div[2]/jhi-central-atendimento/div/form/div/div[7]/div/button[2]'));
    expect(element(botao.getAttribute('button')).to.equal('disabled'));
});



