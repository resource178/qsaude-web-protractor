describe('Primeiro acesso ao Sistema QUSAUDE', function () {
  it('Acessar a tela inicial do portal', function () {

    browser.get('http://localhost:8074/#/login/administrador');

    var esqueceusenha = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div.col-md-12 > button > span'));

    expect(esqueceusenha.getText()).toEqual('Esqueceu sua senha?');


  });

  it('Preencher campos para realizar Login', function () {

    var usuario = element(by.id('username'));
    var senha = element(by.id('password'));
    var btnEntrar = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div.row > div > div.row.justify-content-center > div > form > button > span'));

    usuario.sendKeys('admin');
    senha.sendKeys('admin');

    expect(btnEntrar.getText()).toEqual('Entrar');
    btnEntrar.click();

    browser.manage().timeouts().implicitlyWait(50000);

  });
  it('Acessar menu de consulta da operadora', function () {
    var operadora = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item > div > li > a > span > span'));
    var consulta = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item > div > li > div > ul > jhi-side-navbar-item:nth-child(3) > div > li > a'));

    expect(operadora.getText()).toEqual('Operadora')
    operadora.click();

    expect(consulta.getText()).toEqual('Consultar')
    consulta.click();

    browser.manage().timeouts().implicitlyWait(50000);

  });
  it('Realizar consulta - Validar aba de Atributos', function () {
    var visualizar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-consulta > div > div:nth-child(5) > div > p-datatable > div > div.ui-datatable-tablewrapper.ng-star-inserted > table > tbody > tr:nth-child(3) > td.columnAcoes.ng-star-inserted > span.ui-cell-data.ng-star-inserted > p-button > button'));
    var tela = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.row > div > h2 > span'));

    visualizar.click();

    expect(tela.getText()).toEqual('Cadastro de Operadora')

    browser.manage().timeouts().implicitlyWait(50000);

  });
  it('Realizar consulta - Validar aba de Endereço', function () {
    var endereco = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.tabOperadora > p-tabview > div > ul > li:nth-child(2)'));

    expect(endereco.getText()).toEqual('Endereço');
    endereco.click();

    browser.manage().timeouts().implicitlyWait(100000);

  });
  it('Realizar consulta - Validar aba de Contato', function () {
    var contato = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.tabOperadora > p-tabview > div > ul > li:nth-child(3)'));
    var tela = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.row > div > h2 > span'));

    contato.click();
    browser.manage().timeouts().implicitlyWait(50000);
    expect(tela.getText()).toEqual('Cadastro de Operadora')



  });
  it('Realizar consulta - Validar aba de Beneficiários', function () {
    var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.tabOperadora > p-tabview > div > ul > li:nth-child(4)'));

    expect(beneficiario.getText()).toEqual('Beneficiário');
    beneficiario.click();

    browser.manage().timeouts().implicitlyWait(50000);

  });
  it('Realizar consulta - Validar aba de Formulários a enviar', function () {
    var formulario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.tabOperadora > p-tabview > div > ul > li:nth-child(5)'));

    expect(formulario.getText()).toEqual('Formulários a Enviar');
    formulario.click();

    browser.manage().timeouts().implicitlyWait(50000);

  });
  it('Realizar consulta - Validar aba de Documentação para contratar', function () {
    var documentacao = element(by.css('#ui-tabpanel-5-label'));
    var tela = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-operadora-form > div > form > div.row > div > h2 > span'));

    documentacao.click();
    expect(tela.getText()).toEqual('Cadastro de Operdora')

    browser.manage().timeouts().implicitlyWait(50000);

  });


});



