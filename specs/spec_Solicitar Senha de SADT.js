var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var reporter = require('./myReporter.js')
var object = require('./objects.js')

//*********************************************************************************************************************************************
 var btnNovaSADT = element(by.attr('jhitranslate', 'qSaudeWebApp.senha.home.novaSPSADT')).element(by.xpath('ancestor::button'));
                                                    
 //Botão Gravar Guia
 btnGravarGuia = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > button'));

 //Botão Gerar Guia
var btnGerarGuia = element(by.attr('jhitranslate', 'qSaudeWebApp.atendimento.acoes.gerar')).element(by.xpath('ancestor::button'));

//********************************************************************************************************************************************

describe('Solicitar senha de SADT', function () {
    it('Clicar na aba Beneficiário', function () {
        var modulo = element(by.id('menu')
    );
        modulo.click();

        browser.manage().timeouts().implicitlyWait(5000000);

        var central = element(by.css('#menu > ul > li:nth-child(1) > a'));

        central.click();

        var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));

        beneficiario.click();

        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.manage().timeouts().implicitlyWait(1000000);
    });
    
        it('Preencher os campos para realizar pesquisa', function () {
            var carteirinha = element(by.css('#float-input-carterinha'));
            var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
            var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div.form-group.row > div.col-md-5.col-sm-12.text-md-right.ng-star-inserted > span.labelInfoElegivel.ng-star-inserted > span'));
           
            carteirinha.click();
            browser.manage().timeouts().implicitlyWait(5000000);
            carteirinha.sendKeys('050290000002008');
           
            btnBuscar.click();
            browser.manage().timeouts().implicitlyWait(5000000);
               
            expect(beneficiario.getText()).toEqual('Beneficiário Elegível');         
        });


        it('Clicar na aba senha', function () {
            browser.sleep(3 * 1000);
            var abaSenha = element(by.css('#ui-tabpanel-1-label > span.ui-tabview-title'));
                        
            abaSenha.click();
            
            browser.sleep(3 * 1000);
            expect(btnNovaSADT.isDisplayed()).toBe(true);
        });

        it('Clicar no botão Nova SP/SADT', function () {
            var registro = element(by.css('#prestador'));
            btnNovaSADT.click();
            browser.sleep(5 * 1000);
            //Comando para permanecer na página do Novo Atendimento
            browser.get('https://hml-autorizacao.qsaude.com.br/#/novo-atendimento-sadt?beneficiario=050290000002008');
            browser.sleep(5 * 1000);
            expect(registro.isDisplayed()).toBe(true);         
        });

        it('Preencher os campos Novo Atendimento e clicar em Gravar Guia', function () {
            numGuiaPrestador = element(by.css('#numGuiaPrestador'));
            nomePrestador = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > div:nth-child(1) > jhi-prestador-solicitante-ac > div.col-md-10 > span > p-autocomplete > span > input'));
            profissionalExecutante = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > div:nth-child(1) > jhi-profissional-solicitante-ac > div:nth-child(3) > div.col-md-7 > span > p-autocomplete > span > input'));
            contratadoExecutante = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > div:nth-child(1) > jhi-contratado-executante-ac > div.col-md-8 > span > p-autocomplete > span > input'));
            cboCaraterAtendimento = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > div:nth-child(1) > div:nth-child(18) > div:nth-child(2) > p-dropdown > div > label'));
            itemCaraterAtendimento = element(by.xpath('/html/body/jhi-main/div[2]/jhi-painel/div/div/form/div[1]/div[6]/div[2]/p-dropdown/div/div[4]/div/ul/li[2]/span'));
            codigo = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > div:nth-child(1) > jhi-itens-ac > div:nth-child(2) > span > p-autocomplete > span > input'));
            quantidade = element(by.css('#quantidade'));
            btnMais = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-painel > div > div > form > div:nth-child(1) > jhi-itens-ac > div.col-md-1.fix-input.ng-star-inserted > button'));
            btnPasta = element(by.xpath('/html/body/jhi-main/div[2]/jhi-painel/div/jhi-sadt-menu/div/div/div/div[2]/a/i'));
           
            //Preenchendo os campos para novo atendimento
            numGuiaPrestador.sendKeys('1234');
            nomePrestador.sendKeys('LABORATORIO TOSTES ANALISES CLIN E ANAT PATOL');
            browser.sleep(3 * 1000);
            contratadoExecutante.sendKeys('CIMEDICA-CENTRO DE IMAGENOLOGIA MEDICA S/S LTDA');
            browser.sleep(3 * 1000);
            cboCaraterAtendimento.click();
            browser.manage().timeouts().implicitlyWait(6000000);
            itemCaraterAtendimento.click();
            codigo.sendKeys('20202016');
            browser.sleep(3 * 1000);
            quantidade.sendKeys('200');
            browser.sleep(3 * 1000);
            btnMais.click();
            
            //Clicando o botão Gravar Guia
            btnGravarGuia.click();
            browser.sleep(5 * 1000);
            expect(btnGerarGuia.isDisplayed()).toBe(true);           
        });

        it('Clicar no botão Gerar Guia', function () {
            var status = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-atendimento-visualizar-sadt > div > div > form > div > div:nth-child(3) > div > div > strong'));
            btnGerarGuia.click();
            browser.sleep(3 * 1000);
            expect(status.getText()).toEqual('Em Análise');      
        });
    });  
