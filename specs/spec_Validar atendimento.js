var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var report = require('./myReporter.js')
var object = require('./objects.js')

describe('Validar Autorização Q Saude', function () {
it('Clicar a aba Beneficiário', function () {

    var modulo = element(by.id('menu'));
    var central = element(by.css('#menu > ul > li:nth-child(1) > a'));
    var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));
    var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
    var btnNovo = element(by.attr('jhitranslate', 'qSaudeWebApp.central-atendimento.botao.novo')).element(by.xpath('ancestor::button'));

    modulo.click();
    central.click();
    beneficiario.click();

    expect(btnNovo).toBeDisabled();

});
it('Realizar uma pesquisa de um beneficiário', function () {

    var carteirinha = element(by.css('#float-input-carterinha'));
    var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

    carteirinha.click();
    carteirinha.sendKeys('050290000002008');

    btnBuscar.click();
    expect(btnBuscar.getText()).toEqual('Buscar');

});
it('Clicar no botão Novo ', function () {

    var btnNovo = element(by.attr('jhitranslate', 'qSaudeWebApp.central-atendimento.botao.novo')).element(by.xpath('ancestor::button'));

    btnNovo.click();
    expect(btnNovo.getText()).toEqual('Novo');

    browser.sleep(5 * 1000);
});

it('Preencher os campos e clicar em Salvar ', function () {

    var comboAssunto = element(by.xpath('/html/body/jhi-main/div[2]/jhi-central-main/div/div/div[2]/jhi-central-atendimento/div/form/div/div[2]/div/div[1]/p-dropdown/div/label'));
    var assunto = element(by.xpath('/html/body/jhi-main/div[2]/jhi-central-main/div/div/div[2]/jhi-central-atendimento/div/form/div/div[2]/div/div[1]/p-dropdown/div/div[4]/div[2]/ul/li[1]'));
    var comboEvento = element(by.xpath('/html/body/jhi-main/div[2]/jhi-central-main/div/div/div[2]/jhi-central-atendimento/div/form/div/div[3]/p-dropdown/div/label'));
    var evento = element(by.xpath('/html/body/jhi-main/div[2]/jhi-central-main/div/div/div[2]/jhi-central-atendimento/div/form/div/div[3]/p-dropdown/div/div[4]/div/ul/li'));
    var observacao = element(by.name('descricao'));
    var btnSalvar = element(by.css('/html/body/jhi-main/div[2]/jhi-central-main/div/div/div[2]/jhi-central-atendimento/div/form/div/div[8]/div/button[2]'));
    var check1 = element(by.xpath('/html/body/jhi-main/div[2]/jhi-central-main/div/div/div[2]/jhi-central-atendimento/div/form/div/div[5]/p-checkbox/div/div[2]'));
    var check2 = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-3.no-padding-left > jhi-central-atendimento > div > form > div > div:nth-child(6) > p-checkbox > div > div.ui-chkbox-box.ui-widget.ui-corner-all.ui-state-default'));
    var btnSalvar = element(by.attr('jhitranslate', 'qSaudeWebApp.central-atendimento.botao.salvar')).element(by.xpath('ancestor::button'));

    comboAssunto.click();
    assunto.click();

    comboEvento.click();
    evento.click();

    check1.click();
    check2.click();

    observacao.click();
    observacao.sendKeys('Automação de testes - QSaude.');

    btnSalvar.click();
    expect(btnSalvar.getText()).toEqual('Salvar');

    browser.sleep(5 * 1000);
});
it('Validar Histórico de Atendimento', function () {

    var historico = element(by.id('ui-tabpanel-5-label'));
    var grid = element(by.id('ui-tabpanel-5'));

    historico.click();
    browser.sleep(5 * 1000);

    expect(grid).toBeDisplayed();
    browser.sleep(5 * 1000);
});
});



