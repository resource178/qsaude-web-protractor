describe('Validar Login Q Saúde', function () {
    it('Acessar a tela inicial do Q Saude', function () {
        browser.waitForAngularEnabled(false);
        browser.get('https://hml-autorizacao.qsaude.com.br/');
        var qsaude = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > h5 > span'));

        expect(qsaude.getText()).toEqual('Autenticação');

    });


    it('Preencher campos para realizar login com dados inválidos', function () {
        var btnLogin = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > div.row.justify-content-center > div > form > button'));
        var usuario = element(by.css('#username'));
        var senha = element(by.css('#password'));
               
        usuario.sendKeys('teste');
        senha.sendKeys('teste');

        browser.sleep(2 * 1000);   

        expect(btnLogin.getText()).toEqual('Entrar');
         
    });

    it('Validar mensagem de erro', function () {
        var btnLogin = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > div.row.justify-content-center > div > form > button'));
        var msgErro = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.row.justify-content-center > div > div > span > strong'));
        
        btnLogin.click();
        browser.sleep(2 * 1000);
        expect(msgErro.getText()).toEqual('Erro de autenticação!');

    });   
});