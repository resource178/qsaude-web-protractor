describe('Validar Login Q Saude', function () {
    it('Acessar a tela inicial do Q Saude', function () {
        browser.waitForAngularEnabled(false);
        browser.get('https://hml-autorizacao.qsaude.com.br/');
        var qsaude = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > h5 > span'));

        expect(qsaude.getText()).toEqual('Autenticação');

    });


    it('Preencher campos para realizar login', function () {

        var usuario = element(by.css('#username'));
        var senha = element(by.css('#password'));
        var btnLogin = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > div.row.justify-content-center > div > form > button'));


        usuario.sendKeys('admin');
        senha.sendKeys('admin');

        browser.manage().timeouts().implicitlyWait(1000000);

        expect(btnLogin.getText()).toEqual('Entrar');
        
        btnLogin.click();

    });

    it('Validar login', function () {
        
        var comboOperadora =  element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(2) > div > div > h5 > span'));
        browser.sleep(2 * 1000);
        expect(comboOperadora.getText()).toEqual('Autenticação');
    
    });

   
});