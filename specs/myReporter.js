﻿var myReporter = {
    
    jasmineStarted: function (suiteInfo) {
        console.log('Executando suite com ' + suiteInfo.totalSpecsDefined);
    },


    suiteStarted: function (result) {

        console.log('Suite iniciada: ' + result.description  + ' cuja descrição completa é: ' + result.fullName);
    },

    specStarted: function (result) {

        console.log('Suite iniciada: ' + result.description  + ' cuja descrição completa é: ' + result.fullName);
    },

    specDone: function (result) {
        console.log('Spec: ' + result.description  + ' foi ' + result.status);
        for (var i = 0; i < result.failedExpectations.length; i++) {

            console.log('Falha: ' + result.failedExpectations[i].message);
            console.log(result.failedExpectations[i].stack);
        }

        console.log(result.passedExpectations.length);
    },

    suiteDone: function (result) {

        console.log('Suite: ' + result.description  + ' foi ' + result.status);
        for (var i = 0; i < result.failedExpectations.length; i++) {

            console.log('AfterAll ' + result.failedExpectations[i].message);
            console.log(result.failedExpectations[i].stack);
        }
    },

    jasmineDone: function () {
        console.log('Suíte concluída.');
    }
};

jasmine.getEnv().addReporter(myReporter);
