describe('Validar seleção de operadora', function () {
it('Selecionar Operadora', function () {

        var selecione =  element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(2) > div > div > div.row.justify-content-center > div > form > div > p-dropdown > div > label'));
        //var operadora2 = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(2) > div > div > div.row.justify-content-center > div > form > div > p-dropdown > div > div.ng-tns-c20-9.ui-dropdown-panel.ui-widget-content.ui-corner-all.ui-shadow.ng-trigger.ng-trigger-panelState > div > ul > li:nth-child(2)'));
    
        var operadora = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(2) > div > div > div.row.justify-content-center > div > form > div > p-dropdown > div > div.ng-tns-c6-0.ui-dropdown-panel.ui-widget-content.ui-corner-all.ui-shadow.ng-trigger.ng-trigger-panelState > div > ul > li:nth-child(2)'));
        var btnEntrar = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(2) > div > div > div.row.justify-content-center > div > form > button'));

        selecione.click();
        operadora.click();

        browser.manage().timeouts().implicitlyWait(1000000);

        expect(btnEntrar.getText()).toEqual('Entrar');
        btnEntrar.click();

    });
});