var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var reporter = require('./myReporter.js')

describe('Validar todas as abas de beneficiário', function () {
    it('Clicar a aba Beneficiário Cadastro', function () {

        var modulo = element(by.id('menu'));

        modulo.click();

        browser.manage().timeouts().implicitlyWait(5000000);

        var central = element(by.css('#menu > ul > li:nth-child(1) > a'));

        central.click();

        var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));

        beneficiario.click();

        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width')), 5000));
    });

    it('Clicar a aba Senha', function () {
        var senha = element(by.css('#ui-tabpanel-1-label > span.ui-tabview-title'));
        senha.click();
        //#ui-panel-0 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-8.ng-star-inserted > span
        var painel = element(by.css('#ui-panel-0 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-8.ng-star-inserted'));

        expect(painel.getText()).toEqual('Filtros');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-panel-0 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-8.ng-star-inserted')), 5000));
       
    });

    it('Clicar a aba Plano/Cobertura', function () {
        var plano = element(by.css('#ui-tabpanel-2-label > span.ui-tabview-title'));
        plano.click();

        var painel = element(by.css('#ui-panel-1 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-17.ng-star-inserted'));
        expect(painel.getText()).toEqual('Informações de Produto / Plano');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-panel-1 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-17.ng-star-inserted')), 5000));
    
    });

    it('Clicar a aba Busca Rede', function () {
        var rede = element(by.css('#ui-tabpanel-3-label > span.ui-tabview-left-icon.fa.fa-search.ng-star-inserted'));
        rede.click();

        var painel = element(by.css('#ui-panel-4 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-20.ng-star-inserted'));
        expect(painel.getText()).toEqual('Filtros');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-panel-4 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-20.ng-star-inserted')), 5000));
    
    });

    it('Clicar a aba Reembolso', function () {
        var rede = element(by.css('#ui-tabpanel-4-label > span.ui-tabview-left-icon.fa.fa-money.ng-star-inserted'));
        rede.click();

        var painel = element(by.css('#ui-tabpanel-4 > jhi-reembolso > div > div > div > table > thead > tr > th:nth-child(1) > span:nth-child(1)'));
        expect(painel.getText()).toEqual('Status');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-tabpanel-4 > jhi-reembolso > div > div > div > table > thead > tr > th:nth-child(1) > span:nth-child(1)')), 5000));
    
    });

    it('Clicar a aba Busca Histórico Atendimento', function () {
        var historico = element(by.css('#ui-tabpanel-5-label > span.ui-tabview-left-icon.fa.fa-list-ul.ng-star-inserted'));
        historico.click();

        var painel = element(by.css('#ui-panel-5 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-26.ng-star-inserted'));
        expect(painel.getText()).toEqual('Filtros');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(
            by.css('#ui-panel-5 > div.ui-panel-titlebar.ui-widget-header.ui-helper-clearfix.ui-corner-all.ng-tns-c23-26.ng-star-inserted')), 5000));
    
    });
});



