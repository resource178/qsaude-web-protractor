var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var object = require('./objects.js')

//*********************************************************************************************************************************************

var btnBuscar = element(by.attr('jhitranslate', 'qSaudeWebApp.beneficiario.filtro.buscar')).element(by.xpath('ancestor::button'));

//********************************************************************************************************************************************


it('Clicar na aba Solucao e Retorno SER', function () {

    var modulo = element(by.id('menu')
    );

    modulo.click();

    browser.manage().timeouts().implicitlyWait(5000000);

    var central = element(by.css('#menu > ul > li:nth-child(1) > a'));

    central.click();

    var solucaoRetorno = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(26) > div > li > a > span > span'));

    solucaoRetorno.click();

    browser.manage().timeouts().implicitlyWait(1000000);

    expect(btnBuscar.getText()).toEqual('Buscar');
    browser.manage().timeouts().implicitlyWait(1000000);
});



it('Realizar uma pesquisa de um beneficiário', function () {

    var carteirinha = element(by.css('#float-input-carterinha'));
    var btnAutorizar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-listagem-atendimento > div > div.pt-4.ng-star-inserted > table > tbody > tr > button:nth-child(10) > i'));

    carteirinha.click();
    browser.manage().timeouts().implicitlyWait(5000000);
    carteirinha.sendKeys('050290000002008');

    browser.manage().timeouts().implicitlyWait(5000000);

    btnBuscar.click();
    browser.sleep(12 * 1000);

    expect(btnAutorizar.isDisplayed()).toBe(true);
    browser.sleep(3 * 1000);

});


it('Clicar no botão Autorizar ', function () {

    var btnAutorizar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-listagem-atendimento > div > div.pt-4.ng-star-inserted > table > tbody > tr > button:nth-child(10) > i'));
    var btnLimpar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-listagem-atendimento > p-dialog > div > div.ui-dialog-content.ui-widget-content > div > jhi-central-atendimento > div > form > div > div:nth-child(9) > div > button.btn.btn-warning.btn-width'));

    btnAutorizar.click();

    browser.sleep(3 * 1000);

    expect(btnLimpar.isDisplayed()).toBe(true);

});

it('Preencher os campos e clicar em Salvar ', function () {
    var comboAssunto = element(by.xpath('/html/body/jhi-main/div[2]/jhi-listagem-atendimento/p-dialog/div/div[2]/div/jhi-central-atendimento/div/form/div/div[3]/div/div[1]/p-dropdown/div/label'));
    var assunto = element(by.xpath('/html/body/jhi-main/div[2]/jhi-listagem-atendimento/p-dialog/div/div[2]/div/jhi-central-atendimento/div/form/div/div[3]/div/div[1]/p-dropdown/div/div[4]/div[2]/ul/li[4]/span'));
    var comboEvento = element(by.xpath("/html/body/jhi-main/div[2]/jhi-listagem-atendimento/p-dialog/div/div[2]/div/jhi-central-atendimento/div/form/div/div[4]/p-dropdown/div/label"));
    var evento = element(by.xpath('/html/body/jhi-main/div[2]/jhi-listagem-atendimento/p-dialog/div/div[2]/div/jhi-central-atendimento/div/form/div/div[4]/p-dropdown/div/div[4]/div/ul/li[2]/span'));
    //var evento = element(by.xpath("//*[@class = 'ui-dropdown-items ui-dropdown-list ui-widget-content ui-widget ui-corner-all ui-helper-reset ng-tns-c6-21 ng-star-inserted']/li[2]/span"));
    var observacao = element(by.name('descricao'));
    var btnSalvar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-listagem-atendimento > p-dialog > div > div.ui-dialog-content.ui-widget-content > div > jhi-central-atendimento > div > form > div > div:nth-child(9) > div > button:nth-child(2)'));
    var btnAutorizar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-listagem-atendimento > div > div.pt-4.ng-star-inserted > table > tbody > tr > button:nth-child(10) > i'));

    browser.sleep(5 * 1000);

    //Preenchendo o combo Assunto
    comboAssunto.click();
    browser.manage().timeouts().implicitlyWait(3000000);
    assunto.click();
    //Preenchendo o combo Evento
    comboEvento.click();
    browser.manage().timeouts().implicitlyWait(3000000);
    evento.click();
    //Preenchendo o campo Observação
    browser.manage().timeouts().implicitlyWait(3000000);
    observacao.click();
    observacao.sendKeys('teste teste teste teste teste');

    //Clicando no botão Salvar
    btnSalvar.click();
    browser.sleep(2 * 1000);

    expect(btnSalvar.isDisplayed()).toBe(true);

});

it('Grid atualizada', function () {
    browser.sleep(20 * 1000);
    expect(btnBuscar.isDisplayed()).toBe(true);

});

it('Grid atualizada', function () {
    expect(btnBuscar.isDisplayed()).toBe(true);
});