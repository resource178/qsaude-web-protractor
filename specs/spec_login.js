var helpers = require('protractor-helpers');
var reporter = require('./myReporter.js')

describe('Validar Login Q Saude', function () {
    it('Acessar a tela inicial do Q Saude', function () {
        browser.waitForAngularEnabled(false);
        browser.get('https://hml-autorizacao.qsaude.com.br/');

        var qsaude = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > h5 > span'));
        browser.driver.manage().window().maximize();
        expect(qsaude.getText()).toEqual('Autenticação');
        browser.sleep(2 * 1000);     
    });


    it('Preencher campos para realizar login', function () {
        var usuario = element(by.css('#username'));
        var senha = element(by.css('#password'));
        var btnLogin = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > div.row.justify-content-center > div > form > button'));
        usuario.sendKeys('admin');
        senha.sendKeys('admin');

        expect(btnLogin.getText()).toEqual('Entrar');
        btnLogin.click();
        browser.sleep(2 * 1000);        
    });
});