var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
    

    it('Clicar a aba Beneficiário', function () {

        var modulo = element(by.id('menu')
    );

        modulo.click();

        browser.manage().timeouts().implicitlyWait(5000000);

        var central = element(by.css('#menu > ul > li:nth-child(1) > a'));

        central.click();

        var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));

        beneficiario.click();

        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.manage().timeouts().implicitlyWait(1000000);
        browser.manage().timeouts().implicitlyWait(1000000);

    });
    
        it('Preencher os campos para realizar pesquisa', function () {
    
            var carteirinha = element(by.css('#float-input-carterinha'));
            var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
            var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div.form-group.row > div.col-md-5.col-sm-12.text-md-right.ng-star-inserted > span.labelInfoElegivel.ng-star-inserted > span'));
           
            carteirinha.click();
            browser.manage().timeouts().implicitlyWait(5000000);
            carteirinha.sendKeys('050290000002008');
           
            btnBuscar.click();
            browser.manage().timeouts().implicitlyWait(5000000);
               
            expect(beneficiario.getText()).toEqual('Beneficiário Elegível');
            browser.manage().timeouts().implicitlyWait(5000000);
            
        });


        it('Validar beneficiário Elegível', function () {
            
            var beneficiario = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div.form-group.row > div.col-md-5.col-sm-12.text-md-right.ng-star-inserted > span.labelInfoElegivel.ng-star-inserted > span'));
            expect(beneficiario.getText()).toEqual('Beneficiário Elegível');
        });

   
