var reporter = require('./myReporter.js')

describe('Solicitar Nova Senha com e-mail cadastrado', function () {
    it('Acessar a tela inicial do Q Saude', function () {
        browser.waitForAngularEnabled(false);
        browser.get('https://hml-autorizacao.qsaude.com.br/');
        var qsaude = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.row > div > h5 > span'));
        expect(qsaude.getText()).toEqual('Autenticação');

    });


    it('Preencher campos e clicar em Esqueceu senha', function () {
        var usuario = element(by.css('#username'));
        var esqueceuSenha = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-login > div.container-fluid.animated.flipInX > div > div:nth-child(1) > div.col-md-12 > button > span'));

        usuario.sendKeys('LIA Guia Q');
               
        browser.manage().timeouts().implicitlyWait(1000000);

        expect(esqueceuSenha.getText()).toEqual('Esqueceu sua senha?');
        
        esqueceuSenha.click();

    });

    it('Solicitar nova senha com email cadastrado', function () {
        
        var comboEmail =  element(by.id('email'));
        var btnCriar = element(by.css('body > jhi-main > div.container-fluid.page-content > jhi-password-reset-init > div > div > div > form > button'));
        var msg =  element(by.xpath('/html/body/jhi-main/div[2]/jhi-password-reset-init/div/div/div/div/p/span'));
        
        comboEmail.sendKeys('enakagawa@gamasaude.com.br')
        btnCriar.click();

        expect(msg.getText()).toEqual('Informe endereço de email utilizado no cadastro.');
    
    });


   
});