by.addLocator('attr',
function (attr, value, parentElement) {
   
   parentElement = parentElement || document;
   
   var nodes = parentElement.querySelectorAll('[' + attr + ']');
   
   return Array.prototype.filter.call(nodes, function (node) {
   
   return (node.getAttribute(attr) === value);
   });

});