var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var object = require('./objects.js')

//*********************************************************************************************************************************************
var btnBuscar = element(by.attr('jhitranslate', 'qSaudeWebApp.beneficiario.filtro.buscar')).element(by.xpath('ancestor::button'));

//********************************************************************************************************************************************
it('Clicar na aba Solucao e Retorno SER', function () {

    var modulo = element(by.id('menu')
    );

    modulo.click();

    browser.manage().timeouts().implicitlyWait(5000000);

    var central = element(by.css('#menu > ul > li:nth-child(1) > a'));

    central.click();

    var solucaoRetorno = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(26) > div > li > a > span > span'));

    solucaoRetorno.click();

    browser.manage().timeouts().implicitlyWait(1000000);

    expect(btnBuscar.getText()).toEqual('Buscar');
    browser.manage().timeouts().implicitlyWait(1000000);
});

it('Realizar uma pesquisa de um beneficiário', function () {
    var carteirinha = element(by.css('#float-input-carterinha'));
    var btnAutorizar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-listagem-atendimento > div > div.pt-4.ng-star-inserted > table > tbody > tr > button:nth-child(10) > i'));

    carteirinha.click();
    browser.manage().timeouts().implicitlyWait(5000000);
    carteirinha.sendKeys('050290000002008');

    browser.manage().timeouts().implicitlyWait(5000000);

    btnBuscar.click();
    browser.sleep(12 * 1000);

    expect(btnAutorizar.isDisplayed()).toBe(true);
    browser.sleep(3 * 1000);

});
