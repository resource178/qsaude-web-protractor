var Login = require('./spec_login.js')
var selecionaOperadora = require('./spec_selecionarOperadora.js')
var reporter = require('./myReporter.js')
var object = require('./objects.js')

describe('Validar a aba de cadastro', function () {
    it('Clicar a aba Beneficiário Cadastro', function () {
        var modulo = element(by.id('menu'));
        modulo.click();

        var central = element(by.css('#menu > ul > li:nth-child(1) > a'));
        central.click();

        var beneficiario = element(by.css('#sidebar-wrapper > ul > jhi-side-navbar-item:nth-child(13) > div > li > a'));
        beneficiario.click();

        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));
        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.sleep(2 * 1000);       
       // browser.wait(protractor.ExpectedConditions.visibilityOf(element(
       //     by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width')), 5000));
    });


    it('Realizar uma pesquisa de um beneficiário', function () {
        var carteirinha = element(by.css('#float-input-carterinha'));
        var btnBuscar = element(by.css('body > jhi-main > div.container-fluid.page-content.open > jhi-central-main > div > div > div.col-md-9 > div:nth-child(2) > jhi-beneficiario-filtro > div > form > div > div > div:nth-child(4) > div > button.btn.btn-primary.btn-width'));

        carteirinha.click();
        carteirinha.sendKeys('050290000002008');

        btnBuscar.click();
        expect(btnBuscar.getText()).toEqual('Buscar');
        browser.sleep(2 * 1000);       
        //browser.wait(protractor.ExpectedConditions.visibilityOf(element(
         //   by.attr('id', 'float-input-nomeMae')), 5000));

    });


    it('Validar campos preenchidos', function () {
        var produto = element(by.attr('id', 'float-input-nomeMae'));
        browser.sleep(5 * 1000);
        expect(produto.getAttribute('ng-reflect-model')).toBe('MAE DE JEFFERSON DA SILVA PIRE');
        browser.sleep(2 * 1000);        
        //browser.wait(protractor.ExpectedConditions.visibilityOf(element(
        //    by.attr('id', 'float-input-nomeMae')), 5000));
    });
});
