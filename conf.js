
// protractor.conf.js
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
//var SpecReporter = require('jasmine-spec-reporter');
module.exports.config = {

  suites: {
    smoke: 'spec/smoketests/*.js',
    full: 'spec/full/*.js'
  },

  seleniumAddress: 'http://localhost:4444/wd/hub',

  capabilities: {
    'browserName': 'chrome',
  
    acceptInsecureCerts: true,
    chromeOptions: {
      args: [ "--headless" ]
    },
    
    shardTestFiles: true,
    clsmaxInstances: 1
  },

  specs: [
    'specs/spec_falha no login.js',
    'specs/spec_login.js',
    'specs/spec_solicitar Nova Senha com e-mail cadastrado.js',
    'specs/spec_solicitar Nova Senha com e-mail não cadastrado.js',
    'specs/spec_Solicitar Senha de SADT.js',
    'specs/spec_validar aba cadastro.js',
    'specs/spec_validar aba plano.js',
    'specs/spec_validar aba senha.js',
    'specs/spec_validar abas beneficiario.js',
    'specs/spec_Validar atendimento.js',
    'specs/spec_Validar autorização.js',
    'specs/spec_validar beneficiário elegível.js',
    'specs/spec_validar beneficiário inelegível.js',
    'specs/spec_Solução e Retorno – Buscar Registros.js',
    'specs/spec_Solução e Retorno – Finalizar Ocorrência.js',
  ],

  baseUrl: 'http://www.protractortest.org/',
  onPrepare: function () {
    //var capabilities = config.capabilities;
    var today = new Date();
    var timeStamp = today.getMonth() + 1 + '-' + today.getDate() + '-' + today.getFullYear() + '-' + today.getHours() + 'h-' + today.getMinutes() + 'm-' + today.getSeconds() + 's';

    jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
      displayFailuresSummary: true,
      displayFailedSpec: true,
      displaySuiteNumber: true,
      displaySpecDuration: true,
      cleanDestination:  false,
      savePath: './test/reports/',
      fileNamePrefix: timeStamp,

    }));

    jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
      takeScreenshots: true,
      fixedScreenshotName: true,
    }));
  }
};

